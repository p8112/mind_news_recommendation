import torch
import torch.nn as nn
from torch import Tensor
import math
from typing import Optional


class MultiHeadsAttention(nn.Module):
    """
    Multi heads attention layer
    """
    def __init__(self, num_heads: int, in_features: int, out_features: int):
        """
        Init method
        :param num_heads: number of attention heads
        :param in_features: input features
        :param out_features: output features
        """
        assert out_features % num_heads == 0
        super(MultiHeadsAttention, self).__init__()

        self.num_heads: int = num_heads
        self.out_features: int = out_features
        self.head_features: int = int(out_features / num_heads)
        self.scale_factor: float = math.sqrt(self.head_features)

        self.q_project = nn.Linear(in_features=in_features, out_features=out_features)
        self.k_project = nn.Linear(in_features=in_features, out_features=out_features)
        self.v_project = nn.Linear(in_features=in_features, out_features=out_features)

    def split_heads(self, x: Tensor) -> Tensor:
        """
        Split heads data
        :param x: batch size, length, out_features
        :return: batch size, heads, length, head features
        """
        batch_size, length = x.shape[:2]
        x = x.reshape(batch_size, length, self.num_heads, self.head_features)
        return x.permute(0, 2, 1, 3)

    def merge_heads(self, x: Tensor) -> Tensor:
        """
        Merge heads data
        :param x: batch size, heads, length, head features
        :return: batch size, length, out features
        """
        batch_size, length = x.shape[0], x.shape[2]
        x = x.permute(0, 2, 1, 3)
        return x.reshape(batch_size, length, self.out_features)

    def forward(
            self, query: Tensor, key: Optional[Tensor] = None,
            value: Optional[Tensor] = None, mask: Optional[Tensor] = None
    ) -> Tensor:
        """
        Compute multi head self attention
        :param query: batch_size, q_length, in features
        :param key: batch_size, k_length, in features
        :param value: batch_size, k_length, in features
        :param mask: batch size, q_length, k_length
        :return: batch size, q_length, out features
        """
        key = self.k_project(query) if key is None else self.k_project(key)
        value = self.v_project(query) if value is None else self.v_project(value)
        query = self.q_project(query)

        query = self.split_heads(query)
        key = self.split_heads(key)
        value = self.split_heads(value)

        score = torch.matmul(query, key.transpose(dim0=2, dim1=3))
        score = score / self.scale_factor
        score = torch.exp(score)
        if mask is not None:
            score = score * mask.unsqueeze(dim=1)
        score = score / (score.sum(dim=3, keepdims=True) + 1e-8)

        output = torch.matmul(score, value)
        return self.merge_heads(x=output)
