from torchmetrics.retrieval import RetrievalMRR, RetrievalNormalizedDCG
from torchmetrics.classification import BinaryAUROC
from torch import Tensor
from typing import Dict, List
import torch


class RecommendEvaluator:
    """
    Evaluate recommend result
    """
    def __init__(self, list_k: List[int]):
        """
        Init method
        :param list_k: list top k to compute ndcg
        """
        self.auc_func = BinaryAUROC(ignore_index=-100)
        self.mrr_func = RetrievalMRR(ignore_index=-100)
        self.k_to_ndcg_k_func = {
            k: RetrievalNormalizedDCG(k=k, ignore_index=-100) for k in list_k
        }

    def compute_metrics(self, scores: Tensor, labels: Tensor) -> Dict[str, Tensor]:
        """
        Compute metrics
        :param scores: batch_size, num_posts
        :param labels: batch_size, num_posts
        :return: dict contains
        """
        batch_size, num_interactions = scores.shape
        indexes = torch.arange(start=0, end=batch_size, dtype=torch.long).unsqueeze(dim=1).repeat(
            1, num_interactions
        ).to(scores.device)

        mask = labels != -100
        scores = scores[mask]
        labels = labels[mask]
        indexes = indexes[mask]

        metric_to_value: Dict[str, Tensor] = {
            "auc": self.auc_func(preds=scores, target=labels),
            "mrr": self.mrr_func(preds=scores, target=labels, indexes=indexes)
        }
        for k, ndcg_k_func in self.k_to_ndcg_k_func.items():
            metric_to_value[f"ndcg{k}"] = ndcg_k_func(preds=scores, target=labels, indexes=indexes)

        return metric_to_value
