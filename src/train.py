from data_modules import MindNewsRecommendDataModule
from models import PretrainedBertMultiHeadsSelfAttentionNewsRecommend
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
import torch
from fire import Fire


def main(
        data_dir: str = "data/mind", pretrained_model_name_or_path: str = "prajjwal1/bert-tiny",
        model_max_length: int = 512, max_history_length: int = 50, num_negative: int = 4,
        train_batch_size: int = 16, eval_batch_size: int = 16, num_workers: int = 0,
        hidden_dim: int = 256, num_heads: int = 16, num_layers: int = 1, drop_out: float = 0.01,
        query_vector_dim: int = 200, learning_rate: float = 0.0001, weight_decay: float = 0.0001,
        output_dir: str = "data/model", patience: int = 6, accelerator: str = "gpu",
        max_epochs: int = 5, val_check_interval: float = 0.5, accumulate_grad_batches: int = 8,
        gradient_clip_val: float = 1.0
):
    data_module = MindNewsRecommendDataModule(
        train_dir=f"{data_dir}/train", dev_dir=f"{data_dir}/dev", pretrained_model_name_or_path=pretrained_model_name_or_path,
        model_max_length=model_max_length, max_history_length=max_history_length, num_negative=num_negative,
        train_batch_size=train_batch_size, eval_batch_size=eval_batch_size, num_workers=num_workers
    )
    model = PretrainedBertMultiHeadsSelfAttentionNewsRecommend(
        hidden_dim=hidden_dim, pretrained_model_name_or_path=pretrained_model_name_or_path,
        num_heads=num_heads, num_layers=num_layers, drop_out=drop_out, query_vector_dim=query_vector_dim,
        learning_rate=learning_rate, weight_decay=weight_decay
    )
    trainer = Trainer(
        logger=TensorBoardLogger(save_dir=f"{output_dir}/log"),
        callbacks=[
            ModelCheckpoint(dirpath=output_dir, filename="model", verbose=True, monitor="val_ndcg3", mode="max"),
            EarlyStopping(monitor="val_ndcg3", verbose=True, mode="max", patience=patience)
        ],
        accelerator=accelerator, max_epochs=max_epochs, val_check_interval=val_check_interval,
        accumulate_grad_batches=accumulate_grad_batches, gradient_clip_val=gradient_clip_val
    )
    trainer.fit(model=model, datamodule=data_module)
    model.load_state_dict(state_dict=torch.load(f"{output_dir}/model.ckpt")["state_dict"])
    trainer.test(model=model, datamodule=data_module)


if __name__ == "__main__":
    Fire(main)
