from torch import Tensor
from torch.nn.functional import logsigmoid


def compute_bpr_loss(scores: Tensor, labels: Tensor) -> Tensor:
    """
    Compute bpr loss
    In each instance, first element is positive, the rest is negative
    :param scores: model predicted scores
    :param labels: ground truth labels
    :return: loss
    """
    assert scores.shape == labels.shape

    positive = scores[:, 0]
    positive = positive.unsqueeze(dim=1)
    negative = scores[:, 1:]

    diff = positive - negative
    inv_loss = logsigmoid(diff)
    inv_loss = inv_loss.sum()
    loss = - inv_loss

    return loss
