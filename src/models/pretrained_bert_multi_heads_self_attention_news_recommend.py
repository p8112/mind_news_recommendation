from pytorch_lightning import LightningModule
from .news_encoders import PretrainedBertNewsEncoder
from .user_encoders import MultiHeadsSelfAttentionUserEncoder
from .losses import compute_bpr_loss
from torch import Tensor
from typing import Dict
from .evaluators import RecommendEvaluator
from torch.optim import Adam


class PretrainedBertMultiHeadsSelfAttentionNewsRecommend(LightningModule):
    """
    News recommend model using pretrained bert and multi heads self attention
    """
    def __init__(
            self, hidden_dim: int, pretrained_model_name_or_path: str,
            num_heads: int, num_layers: int, drop_out: float, query_vector_dim: int,
            learning_rate: float, weight_decay: float
    ):
        """
        Init method
        :param hidden_dim: hidden dim
        :param pretrained_model_name_or_path: name of pretrained model to encode news
        :param num_heads: num heads to encode user
        :param num_layers: num layers to encode user
        :param drop_out: drop out
        :param query_vector_dim: dim of query vector in additive attention
        :param learning_rate: learning rate
        :param weight_decay: weight_decay
        """
        super(PretrainedBertMultiHeadsSelfAttentionNewsRecommend, self).__init__()
        self.save_hyperparameters()

        self.news_encoder = PretrainedBertNewsEncoder(
            pretrained_model_name_or_path=pretrained_model_name_or_path, out_features=hidden_dim
        )
        self.user_encoder = MultiHeadsSelfAttentionUserEncoder(
            in_features=hidden_dim, num_heads=num_heads, num_layers=num_layers,
            drop_out=drop_out, query_vector_dim=query_vector_dim
        )

        self.learning_rate: float = learning_rate
        self.weight_decay: float = weight_decay

        self.recommend_evaluator = RecommendEvaluator(list_k=[3, 5, 10])

    def compute_news_encode(self, input_ids: Tensor, attention_mask: Tensor) -> Tensor:
        """
        Compute encode for news
        :param input_ids: batch size, length
        :param attention_mask: batch size, length
        :return: batch size, hidden dim
        """
        return self.news_encoder(input_ids=input_ids, attention_mask=attention_mask)

    def compute_list_news_encodes(self, input_ids: Tensor, attention_mask: Tensor) -> Tensor:
        """
        Compute encode for news
        :param input_ids: batch size, num news, length
        :param attention_mask: batch size, num news, length
        :return: batch size, num news, hidden dim
        """
        batch_size, num_news, length = input_ids.shape
        encodes = self.compute_news_encode(
            input_ids=input_ids.reshape(batch_size*num_news, length),
            attention_mask=attention_mask.reshape(batch_size*num_news, length)
        )
        return encodes.reshape(batch_size, num_news, -1)

    def compute_user_encode(self, input_ids: Tensor, attention_mask: Tensor, history_mask: Tensor) -> Tensor:
        """
        Compute encode of user
        :param input_ids: batch size, num news, length
        :param attention_mask: batch size, num news, length
        :param history_mask: batch size, num news
        :return: batch size, hidden dim
        """
        x = self.compute_list_news_encodes(input_ids=input_ids, attention_mask=attention_mask)
        return self.user_encoder(x=x, mask=history_mask)

    def compute_user_candidates_scores(self, user: Tensor, candidates: Tensor) -> Tensor:
        """
        Compute score for user - candidates
        :param user: batch size, hidden dim
        :param candidates: batch size, num candidates, hidden dim
        :return: batch size, num candidates
        """
        user = user.unsqueeze(dim=1)
        scores = user * candidates
        return scores.sum(dim=2)

    def training_step(self, batch: Dict, *args, **kwargs) -> Tensor:
        """
        Compute loss
        :param batch: dict contains
            - user
                - input_ids
                - attention_mask
                - history_mask
            - candidates
                - input_ids
                - attention_mask
                - labels
        :return:
        """
        candidates = self.compute_list_news_encodes(
            input_ids=batch["candidates"]["input_ids"], attention_mask=batch["candidates"]["attention_mask"]
        )
        user = self.compute_user_encode(
            input_ids=batch["user"]["input_ids"], attention_mask=batch["user"]["attention_mask"],
            history_mask=batch["user"]["history_mask"]
        )

        scores = self.compute_user_candidates_scores(user=user, candidates=candidates)
        loss = compute_bpr_loss(scores=scores, labels=batch["candidates"]["labels"])

        self.log(name="train_loss", value=loss, prog_bar=True)
        return loss

    def forward(self, batch: Dict) -> Tensor:
        """
        Compute recommend score
        :param batch: dict contains
            - user
                - input_ids
                - attention_mask
                - history_mask
            - candidates
                - input_ids
                - attention_mask
        :return:
        """
        candidates = self.compute_list_news_encodes(
            input_ids=batch["candidates"]["input_ids"], attention_mask=batch["candidates"]["attention_mask"]
        )
        user = self.compute_user_encode(
            input_ids=batch["user"]["input_ids"], attention_mask=batch["user"]["attention_mask"],
            history_mask=batch["user"]["history_mask"]
        )

        return self.compute_user_candidates_scores(user=user, candidates=candidates)

    def validation_step(self, batch: Dict, *args, **kwargs) -> None:
        """
        Execute validation
        :param batch: dict contains
            - user
                - input_ids
                - attention_mask
                - history_mask
            - candidates
                - input_ids
                - attention_mask
                - labels
        :return:
        """
        scores = self.forward(batch=batch)
        metric_to_value: Dict[str, Tensor] = self.recommend_evaluator.compute_metrics(
            scores=scores, labels=batch["candidates"]["labels"]
        )
        for metric, value in metric_to_value.items():
            self.log(name=f"val_{metric}", value=value, prog_bar=True)

    def test_step(self, batch: Dict, *args, **kwargs) -> None:
        """
        Execute test
        :param batch: dict contains
            - user
                - input_ids
                - attention_mask
                - history_mask
            - candidates
                - input_ids
                - attention_mask
                - labels
        :return:
        """
        scores = self.forward(batch=batch)
        metric_to_value: Dict[str, Tensor] = self.recommend_evaluator.compute_metrics(
            scores=scores, labels=batch["candidates"]["labels"]
        )
        for metric, value in metric_to_value.items():
            self.log(name=f"test_{metric}", value=value, prog_bar=True)

    def predict_step(self, batch: Dict, batch_idx: int, dataloader_idx: int = 0) -> Tensor:
        """
        Compute predict
        :param batch: dict contains
            - user
                - input_ids
                - attention_mask
                - history_mask
            - candidates
                - input_ids
                - attention_mask
        :param batch_idx:
        :param dataloader_idx:
        :return:
        """
        return self.forward(batch=batch)

    def configure_optimizers(self) -> Adam:
        """
        Get optimizer
        :return:
        """
        return Adam(params=self.parameters(), lr=self.learning_rate, weight_decay=self.weight_decay)
