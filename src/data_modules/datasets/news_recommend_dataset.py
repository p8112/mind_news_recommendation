import random
import torch
from torch.utils.data import Dataset
from abc import ABC, abstractmethod
from typing import List, Dict, Tuple
from transformers import PreTrainedTokenizerBase
from torch import Tensor
from utils import collate_1d_tensors, collate_2d_tensors


class BaseNewsRecommendDataset(Dataset, ABC):
    """
    Base class for news recommend dataset
    """
    def __init__(
            self, instances: List[Dict], news_id_to_title: Dict[str, str],
            tokenizer: PreTrainedTokenizerBase, max_history_length: int
    ):
        """
        Init method
        :param instances: list of data instance, each contains
            - history_news_ids
            - positive_news_ids
            - negative_news_ids
        :param news_id_to_title: mapping from news id to title
        :param tokenizer: tokenizer
        :param max_history_length: max length of history
        """
        super(BaseNewsRecommendDataset, self).__init__()

        self.instances: List[Dict] = instances
        self.news_id_to_title: Dict[str, str] = news_id_to_title
        self.tokenizer: PreTrainedTokenizerBase = tokenizer
        self.max_history_length: int = max_history_length

    def get_list_news_data(self, news_ids: List[str]) -> Dict[str, Tensor]:
        """
        Get data for list of news
        :param news_ids: list of news ids to get data
        :return: dict contains
            - input_ids
            - attention_mask
        """
        titles: List[str] = [self.news_id_to_title[new_id] for new_id in news_ids]
        tmp = self.tokenizer(
            titles, max_length=self.tokenizer.model_max_length, truncation=True, padding=True, return_tensors="pt"
        )
        return {"input_ids": tmp.input_ids, "attention_mask": tmp.attention_mask}

    def get_candidates_data(self, positive_news_ids: List[str], negative_news_ids: List[str]) -> Dict[str, Tensor]:
        """
        Get data for candidates
        :param positive_news_ids: list of positive candidates
        :param negative_news_ids: list of negative candidates
        :return: dict contains
            - input_ids
            - attention_mask
            - labels
        """
        data: Dict[str, Tensor] = self.get_list_news_data(news_ids=positive_news_ids + negative_news_ids)
        data["labels"] = torch.as_tensor([1 for _ in positive_news_ids] + [0 for _ in negative_news_ids], dtype=torch.long)
        return data

    def get_user_data(self, history_news_ids: List[str]) -> Dict[str, Tensor]:
        """
        Get data for user
        :param history_news_ids: list of news in history
        :return: dict contains
            - input_ids
            - attention_mask
            - history_mask
        """
        history_news_ids = history_news_ids[:self.max_history_length]
        if len(history_news_ids) == 0:
            data: Dict[str, Tensor] = self.get_list_news_data(news_ids=["__PADDING__"])
            data["history_mask"] = torch.as_tensor([0.0], dtype=torch.float)
        else:
            data: Dict[str, Tensor] = self.get_list_news_data(news_ids=history_news_ids)
            data["history_mask"] = torch.as_tensor([1.0 for _ in history_news_ids], dtype=torch.float)
        return data

    @abstractmethod
    def get_history_positive_negative_news_ids(self, index: int) -> Dict[str, List[str]]:
        """
        Get history news ids, positive news ids, negative news ids given a data index
        :param index: data index
        :return: dict contains
            - history_news_ids
            - positive_news_ids
            - negative_news_ids
        """
        pass

    def __getitem__(self, index: int) -> Dict:
        """
        Get data instance
        :param index: index of data
        :return: dict contains
            - user:
                - input_ids
                - attention_mask
                - history_mask
            - candidates:
                - input_ids
                - attention_mask
                - labels
        """
        tmp: Dict[str, List[str]] = self.get_history_positive_negative_news_ids(index=index)
        history_news_ids: List[str] = tmp["history_news_ids"]
        positive_news_ids: List[str] = tmp["positive_news_ids"]
        negative_news_ids: List[str] = tmp["negative_news_ids"]

        return {
            "user": self.get_user_data(history_news_ids=history_news_ids),
            "candidates": self.get_candidates_data(positive_news_ids=positive_news_ids, negative_news_ids=negative_news_ids)
        }

    def collate_candidates(self, batch: List[Dict[str, Tensor]]) -> Dict[str, Tensor]:
        """
        Collate data of candidates
        :param batch: list of dict, each contains
            - input_ids
            - attention_mask
            - labels
        :return: dict contains
            - input_ids
            - attention_mask
            - labels
        """
        return {
            "input_ids": collate_2d_tensors(
                batch=[x["input_ids"] for x in batch], pad_value=self.tokenizer.pad_token_id, data_type=torch.long
            ),
            "attention_mask": collate_2d_tensors(
                batch=[x["attention_mask"] for x in batch], pad_value=0, data_type=torch.long
            ),
            "labels": collate_1d_tensors(
                batch=[x["labels"] for x in batch], pad_value=-100, data_type=torch.long
            )
        }

    def collate_user(self, batch: List[Dict[str, Tensor]]) -> Dict[str, Tensor]:
        """
        Collate data of user
        :param batch: list of dict, each contains
            - input_ids
            - attention_mask
            - history_mask
        :return: dict contains
            - input_ids
            - attention_mask
            - history_mask
        """
        return {
            "input_ids": collate_2d_tensors(
                batch=[x["input_ids"] for x in batch], pad_value=self.tokenizer.pad_token_id, data_type=torch.long
            ),
            "attention_mask": collate_2d_tensors(
                batch=[x["attention_mask"] for x in batch], pad_value=0, data_type=torch.long
            ),
            "history_mask": collate_1d_tensors(
                batch=[x["history_mask"] for x in batch], pad_value=0.0, data_type=torch.float
            )
        }

    def collate_fn(self, batch: List[Dict]) -> Dict:
        """
        Collate batch data
        :param batch: list of dict, each contains
            - user:
                - input_ids
                - attention_mask
                - history_mask
            - candidates:
                - input_ids
                - attention_mask
                - labels
        :return: dict contains
            - user:
                - input_ids
                - attention_mask
                - history_mask
            - candidates:
                - input_ids
                - attention_mask
                - labels
        """
        return {
            "user": self.collate_user(batch=[x["user"] for x in batch]),
            "candidates": self.collate_candidates(batch=[x["candidates"] for x in batch])
        }


class TrainNewsRecommendDataset(BaseNewsRecommendDataset):
    """
    Dataset for training
    """
    def __init__(
            self, instances: List[Dict], news_id_to_title: Dict[str, str],
            tokenizer: PreTrainedTokenizerBase, max_history_length: int, num_negative: int
    ):
        """
        Init method
        :param instances: list of data instance, each contains
            - history_news_ids
            - positive_news_ids
            - negative_news_ids
        :param news_id_to_title: mapping from news id to title
        :param tokenizer: tokenizer
        :param num_negative: number of negative co-training with one positive
        """
        super(TrainNewsRecommendDataset, self).__init__(
            instances=instances, news_id_to_title=news_id_to_title,
            tokenizer=tokenizer, max_history_length=max_history_length
        )

        self.num_negative: int = num_negative
        self.positive_news_ids_instance_indexes: List[Tuple[str, int]] = []
        for index, instance in enumerate(self.instances):
            for positive_news_id in instance["positive_news_ids"]:
                self.positive_news_ids_instance_indexes.append((positive_news_id, index))

    def __len__(self) -> int:
        """
        Get length of dataset
        :return: dataset length
        """
        return len(self.positive_news_ids_instance_indexes)

    def get_history_positive_negative_news_ids(self, index: int) -> Dict[str, List[str]]:
        """
        Get history news ids, positive news ids, negative news ids given a data index
        :param index: data index
        :return: dict contains
            - history_news_ids
            - positive_news_ids
            - negative_news_ids
        """
        positive_news_id, instance_index = self.positive_news_ids_instance_indexes[index]
        instance: Dict = self.instances[instance_index]

        negative_news_ids = instance["negative_news_ids"]
        if len(negative_news_ids) > self.num_negative:
            negative_news_ids = random.sample(population=negative_news_ids, k=self.num_negative)
        elif len(negative_news_ids) < self.num_negative:
            negative_news_ids = random.choices(population=negative_news_ids, k=self.num_negative)

        return {
            "history_news_ids": instance["history_news_ids"],
            "positive_news_ids": [positive_news_id],
            "negative_news_ids": negative_news_ids
        }


class EvalNewsRecommendDataset(BaseNewsRecommendDataset):
    """
    Dataset for evaluate
    """
    def __len__(self) -> int:
        """
        Get length of dataset
        :return:
        """
        return len(self.instances)

    def get_history_positive_negative_news_ids(self, index: int) -> Dict[str, List[str]]:
        """
        Get history news ids, positive news ids, negative news ids given a data index
        :param index: data index
        :return: dict contains
            - history_news_ids
            - positive_news_ids
            - negative_news_ids
        """
        return self.instances[index]
