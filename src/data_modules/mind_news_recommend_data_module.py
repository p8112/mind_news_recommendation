from .datasets import TrainNewsRecommendDataset, EvalNewsRecommendDataset
from torch.utils.data import DataLoader
from transformers import PreTrainedTokenizerBase, AutoTokenizer
from pytorch_lightning import LightningDataModule
from typing import List, Dict
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import train_test_split


class MindNewsRecommendDataModule(LightningDataModule):
    """
    Data module for news recommend
    """
    def __init__(
            self, train_dir: str, dev_dir: str, pretrained_model_name_or_path: str, model_max_length: int,
            max_history_length: int, num_negative: int, train_batch_size: int, eval_batch_size: int, num_workers: int
    ):
        """
        Init method
        :param train_dir: dir store train data
        :param dev_dir: dir store dev data
        :param pretrained_model_name_or_path: name or path of pretrained model
        :param model_max_length: max length of pretrained model context
        :param max_history_length: max length of history
        :param num_negative: number of negative co-training with one positive
        :param train_batch_size: train batch size
        :param eval_batch_size: eval batch size
        :param num_workers: num workers for dataloader
        """
        super(MindNewsRecommendDataModule, self).__init__()
        self.save_hyperparameters(ignore="tokenizer")

        self.train_instances: List[Dict] = self.get_instances(file_name=f"{train_dir}/behaviors.tsv")
        self.train_news_id_to_title: Dict[str, str] = self.get_news_id_to_title(file_name=f"{train_dir}/news.tsv")

        eval_instances: List[Dict] = self.get_instances(file_name=f"{dev_dir}/behaviors.tsv")
        self.val_instances, self.test_instances = train_test_split(eval_instances, train_size=0.5)
        self.eval_news_id_to_title: Dict[str, str] = self.get_news_id_to_title(file_name=f"{dev_dir}/news.tsv")

        self.tokenizer: PreTrainedTokenizerBase = AutoTokenizer.from_pretrained(
            pretrained_model_name_or_path=pretrained_model_name_or_path, model_max_length=model_max_length
        )
        self.max_history_length: int = max_history_length
        self.num_negative: int = num_negative
        self.train_batch_size: int = train_batch_size
        self.eval_batch_size: int = eval_batch_size
        self.num_workers: int = num_workers

    def get_instances(self, file_name: str) -> List[Dict]:
        """
        Get instances
        :param file_name: file tsv store behaviours
        :return: list of dict, each contains
            - history_news_ids
            - positive_news_ids
            - negative_news_ids
        """
        df: pd.DataFrame = pd.read_csv(file_name, sep="\t", header=None, usecols=[3, 4], na_filter=False).astype(str)
        df.columns = ["history", "impression"]

        instances: List[Dict] = []
        progress_bar = tqdm(iterable=df.iterrows(), desc=f"Get instances from file {file_name}")
        for _, row in progress_bar:
            history: str = row["history"].strip()
            if history == "":
                history_news_ids: List[str] = []
            else:
                history_news_ids: List[str] = history.split(" ")
            positive_news_ids: List[str] = []
            negative_news_ids: List[str] = []
            for news_id_label in row["impression"].strip().split(" "):
                news_id, label = news_id_label.split("-")
                if label == "1":
                    positive_news_ids.append(news_id)
                elif label == "0":
                    negative_news_ids.append(news_id)
            instances.append({
                "history_news_ids": history_news_ids,
                "positive_news_ids": positive_news_ids, "negative_news_ids": negative_news_ids
            })
        progress_bar.close()

        return instances

    def get_news_id_to_title(self, file_name: str) -> Dict[str, str]:
        """
        Get mapping from news id to title
        :param file_name: file tsv store behaviours
        :return: mapping from news id to title
        """
        df: pd.DataFrame = pd.read_csv(file_name, sep="\t", header=None, usecols=[0, 3]).astype(str)
        df.fillna(value="", inplace=True)
        df.columns = ["news_id", "title"]

        news_id_to_title: Dict[str, str] = {"__PADDING__": ""}
        progress_bar = tqdm(iterable=df.iterrows(), desc=f"Get news title from file {file_name}")
        for _, row in progress_bar:
            news_id_to_title[row['news_id']] = row['title']
        progress_bar.close()

        return news_id_to_title

    def train_dataloader(self) -> DataLoader:
        """
        Get data loader for train
        :return: data loader
        """
        dataset = TrainNewsRecommendDataset(
            instances=self.train_instances, news_id_to_title=self.train_news_id_to_title,
            tokenizer=self.tokenizer, max_history_length=self.max_history_length, num_negative=self.num_negative
        )
        return DataLoader(
            dataset=dataset, collate_fn=dataset.collate_fn,
            batch_size=self.train_batch_size, num_workers=self.num_workers
        )

    def val_dataloader(self) -> DataLoader:
        """
        Get data loader for validation
        :return: data loader
        """
        dataset = EvalNewsRecommendDataset(
            instances=self.val_instances, news_id_to_title=self.eval_news_id_to_title,
            tokenizer=self.tokenizer, max_history_length=self.max_history_length
        )
        return DataLoader(
            dataset=dataset, collate_fn=dataset.collate_fn,
            batch_size=self.eval_batch_size, num_workers=self.num_workers
        )

    def test_dataloader(self) -> DataLoader:
        """
        Get data loader for test
        :return: data loader
        """
        dataset = EvalNewsRecommendDataset(
            instances=self.test_instances, news_id_to_title=self.eval_news_id_to_title,
            tokenizer=self.tokenizer, max_history_length=self.max_history_length
        )
        return DataLoader(
            dataset=dataset, collate_fn=dataset.collate_fn,
            batch_size=self.eval_batch_size, num_workers=self.num_workers
        )
