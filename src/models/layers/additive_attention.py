import torch
import torch.nn as nn
from torch import Tensor
from typing import Optional


class AdditiveAttention(nn.Module):
    """
    Additive attention layer
    """
    def __init__(self, in_features: int, query_vector_dim: int):
        """
        Init method
        :param in_features: number features of input
        :param query_vector_dim: number features of query vector
        """
        super(AdditiveAttention, self).__init__()

        self.projector = nn.Sequential(
            nn.Linear(in_features=in_features, out_features=query_vector_dim, bias=False),
            nn.Tanh(),
            nn.Linear(in_features=query_vector_dim, out_features=1, bias=False)
        )

    def forward(self, x: Tensor, mask: Optional[Tensor]) -> Tensor:
        """
        Compute context vector
        :param x: batch_size, length, in_features
        :param mask: batch_size, length
        :return: batch_size, in_features
        """
        score = self.projector(x).squeeze(dim=2)
        score = torch.exp(score)
        if mask is not None:
            score = score * mask
        score = score / (score.sum(dim=1, keepdim=True) + 1e-8)

        context = torch.bmm(score.unsqueeze(dim=1), x)
        return context.squeeze(dim=1)
