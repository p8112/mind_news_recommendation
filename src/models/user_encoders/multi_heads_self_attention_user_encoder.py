from models.layers import MultiHeadsAttention, AdditiveAttention
import torch.nn as nn
from torch import Tensor


class MultiHeadsSelfAttentionUserEncoder(nn.Module):
    """
    User encoder using multi head self attention
    """
    def __init__(
            self, in_features: int, num_heads: int, num_layers: int,
            query_vector_dim: int, drop_out: float
    ):
        """
        Init method
        :param in_features: input features
        :param num_heads: number of attention heads
        :param num_layers: number of attention layers
        :param query_vector_dim: query vector dim
        :param drop_out: drop out percentage
        """
        super(MultiHeadsSelfAttentionUserEncoder, self).__init__()

        self.self_attention_layers = nn.ModuleList([
            MultiHeadsAttention(
                in_features=in_features, out_features=in_features, num_heads=num_heads
            )
            for _ in range(num_layers)
        ])
        self.additive_attention = AdditiveAttention(
            in_features=in_features, query_vector_dim=query_vector_dim
        )
        self.drop_layer = nn.Dropout(p=drop_out)

    def forward(self, x: Tensor, mask: Tensor) -> Tensor:
        """
        Compute user encode
        :param x: batch size, length, in features
        :param mask: batch size, length
        :return: batch size, in features
        """
        length: int = mask.shape[1]
        mask_expand = mask.unsqueeze(dim=1).expand(-1, length, -1)
        for layer in self.self_attention_layers:
            x = layer(query=x, mask=mask_expand)
            x = self.drop_layer(x)

        return self.additive_attention(x=x, mask=mask)
