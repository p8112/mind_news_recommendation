from torch import Tensor
import torch
from typing import List


def collate_1d_tensors(batch: List[Tensor], pad_value: object, data_type: torch.dtype) -> Tensor:
    """
    Collate 1d tensors
    :param batch: list of 1d tensors
    :param pad_value: padding value
    :param data_type: data type of tensors
    :return: 2d tensor: batch_size, length
    """
    batch_size: int = len(batch)
    max_length: int = max([x.shape[0] for x in batch])
    result = pad_value * torch.ones(size=(batch_size, max_length), dtype=data_type)

    for idx, data in enumerate(batch):
        length: int = data.shape[0]
        result[idx, :length] = data
    return result


def collate_2d_tensors(batch: List[Tensor], pad_value: object, data_type: torch.dtype) -> Tensor:
    """
    Collate 2d tensors
    :param batch: list of 2d tensors
    :param pad_value: padding value
    :param data_type: data type of tensors
    :return: 3d tensor: batch_size, length_1, length_2
    """
    batch_size: int = len(batch)
    max_length_0: int = max([x.shape[0] for x in batch])
    max_length_1: int = max([x.shape[1] for x in batch])
    result = pad_value * torch.ones(size=(batch_size, max_length_0, max_length_1), dtype=data_type)

    for idx, data in enumerate(batch):
        length_0, length_1 = data.shape
        result[idx, :length_0, :length_1] = data
    return result
