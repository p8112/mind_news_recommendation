import torch.nn as nn
from transformers import AutoModel
from torch import Tensor


class PretrainedBertNewsEncoder(nn.Module):
    """
    Using pretrained model to encode news
    """
    def __init__(self, pretrained_model_name_or_path: str, out_features: int):
        """
        Init method
        :param pretrained_model_name_or_path: name or path of pretrained model
        :param out_features: output size
        """
        super(PretrainedBertNewsEncoder, self).__init__()

        self.encoder: nn.Module = AutoModel.from_pretrained(
            pretrained_model_name_or_path=pretrained_model_name_or_path, add_pooling_layer=False
        )
        if self.encoder.config.hidden_size == out_features:
            self.projector = nn.Identity()
        else:
            self.projector = nn.Linear(in_features=self.encoder.config.hidden_size, out_features=out_features)

    def forward(self, input_ids: Tensor, attention_mask: Tensor) -> Tensor:
        """
        Compute news encode
        :param input_ids: batch size, length
        :param attention_mask: batch size, length
        :return: batch size, out features
        """
        output = self.encoder(input_ids=input_ids, attention_mask=attention_mask).last_hidden_state[:, 0, :]
        return self.projector(output)
